import pyslurm
a = pyslurm.config()
ctl_dict = a.get()
for key in sorted(ctl_dict.keys()):
   print("%-35s : %s" % (key, ctl_dict[key]))