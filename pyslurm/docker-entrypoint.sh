#!/bin/bash
set -e
/etc/init.d/rsyslog start
/etc/init.d/munge start 
echo "In the docker entrypoint"
/bin/sh -c "trap : TERM INT; sleep 9999999999d & wait"
exec "$@"