#!/bin/bash
set -e
/etc/init.d/rsyslog start
/etc/init.d/munge start
until echo "SELECT 1" | mysql -h localhost -u slurm -ppassword 2>&1 > /dev/null
        do
            echo "-- Waiting for database to become active ..."
            sleep 2
        done
exec slurmdbd -Dvvv
/bin/sh -c "trap : TERM INT; sleep 9999999999d & wait"
exec "$@"